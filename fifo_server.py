import os

path1 = "./request.fifo"
path2 = "./result.fifo"
os.mkfifo(path1)
os.mkfifo(path2)
f1 = open(path1, "r")
f3 = open(path2, "w")
filename = f1.readline()
f2 = open(filename, "r")
for line in f2:
  f3.write(line)
f1.close()
f2.close()
f3.close()
os.unlink(path1)
os.unlink(path2)