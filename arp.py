import json

print("ARP")
ip = input("Enter IP address: ")
with open('resolver.txt') as infile:
  data = json.load(infile)
try:
  print("Corresponding MAC address is ", data[ip])
except:
  print("Corresponding MAC not found!")

print("RARP")
res = "IP adress Not Found!"
mac = input("Enter MAC address : ")
for key, value in data.items():
  if value == mac:
    res = key
print("Corresponding IP address is ", res)